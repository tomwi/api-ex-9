<?php
class Controllers_Users extends RestController {
	private $_con;
	private function DBconnect(){
		$this->_con=mysqli_connect("localhost","tomwi_tomwi_api","33797","tomwi_api");
	}
	public function get() {
		$this->DBconnect();
		//var_dump($this->request['params']['id']);
		//die();
		if (isset($this->request['params']['id'])){
			if(!empty($this->request['params']['id'])) {
				$sql="SELECT * FROM users WHERE id=?";				
				$stmt = $this->_con->prepare($sql);
				$stmt->bind_param("s",$this->request['params']['id']); //corresponding variable has type string
				$stmt->execute();
				$stmt->store_result();				
				$stmt->bind_result($id, $name,$email);
				$stmt->fetch();
				$result = ['id'=>$id,'name'=>$name,'email'=>$email];
				$this->response = array('result' =>$result );
				$this->responseStatus = 200;
			} else {
				//in case that in the URL there isn't id- pulling out all the users list
				$sql="SELECT * FROM users";
				$usersSqlResult=mysqli_query($this->_con,$sql);
				$users=[];
				while ($user=mysqli_fetch_array($usersSqlResult)){
					$users[]=['name'=>$user['name'],'email'=>$user['email'],'id'=>$user['id']];//adds values to array 'users'
				}
				$this->response=array('result'=>$users);//the response is an array that every user is an array by itself.
			}	
		}else{
			$this->response = array('result' =>'Wrong parameters for users' );
			$this->responseStatus = 200;			
		}
	}
	public function post() {
		$this->DBconnect();
				$user  = json_decode($this->request['params']['payload'],true);
				$name=$user[name];
				$email=$user[email];
				$sql=mysqli_query($this->_con,"INSERT INTO `users` (`id`,`name`, `email`) VALUES ('NULL','$name','$email')");
				$sql2="SELECT * FROM `users` ORDER BY `id` DESC LIMIT 1";
				$result = mysqli_query($this->_con, $sql2);
				$users = [];
				while($user = mysqli_fetch_array($result)){
				$id = ['id' => $user['id']];
				}
				echo 'the last id is :';
				 $this->response = array('result' => $id);

				
			//}
			//else{
			//	$this->response = array('result' => 'no post implemented for users');
			//	$this->responseStatus = 201;
			//}
	}
	
	public function put() {
		$this->response = array('result' => 'no put implemented for users');
		$this->responseStatus = 200;
	}
	public function delete() {
		$this->response = array('result' => 'no delete implemented for users');
		$this->responseStatus = 200;
	}
}